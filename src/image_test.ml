open Base
open Image

let () =
  let image = Matrix.make_fun 200 100 (fun x y -> Rgb.of_percent ((Float.of_int x) /. 200.,
                                                                  0.5,
                                                                  (Float.of_int y) /. 100.)) in
  draw_image (make image) "test.ppm"
  (**  display_image (make image) 5 **)
