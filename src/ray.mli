(** Ray(x) = origin_vector + x * direction_vector **) 
type t

(** Create ray. **)
val make : Vec3.t -> Vec3.t -> t

(** Create ray with direction vector transformed into unit vector. **)
val make_unit : Vec3.t -> Vec3.t -> t
                                     
(** Get origin vector of ray. **)
val origin : t -> Vec3.t

(** Get direction vector of ray. **)
val direction : t -> Vec3.t

(** Get point Ray(float). **)
val point_at_parameter : t -> float -> Vec3.t

(** Return true if two rays are equal, else false. **)                                
val (=!) : t -> t -> bool
