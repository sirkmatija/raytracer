open Base
open Vec3

let () =
  let vector1 = make 0.5 1. 2.
  and vector1' = make 0.5 1. 2.
  and vector2 = make 3. 2. 4.
  and vector3 = make 1. 0. 0. in
  assert ((Float.to_int (length vector2)) = Float.to_int (Float.sqrt 29.));
  assert ((Float.to_int (x (of_tuple (3., 2., 1.)))) = 3);
  assert ((Float.to_int (z vector3)) = (Float.to_int(y (unit_vec vector3))));
  let a = vector1 +! vector2
  and b = vector1 -! vector2
  and c = dot vector1 vector2
  and d = cross vector1 vector2 in
  assert ((Float.to_int (x a)) = 3);
  assert ((Float.to_int (y b)) = (-1));
  assert ((Float.to_int c) = 11);
  assert ((Float.to_int (z d)) = (-2));
  assert (not (vector1 =! vector2));
  assert (vector1 =! vector1')
