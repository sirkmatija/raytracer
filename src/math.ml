open Base

let solve_quadratic (a: float) (b: float) (c: float) : float list =
  let discriminant = b *. b -. 4. *. a *. c in
  match Float.sign_exn discriminant with
  | Neg -> []
  | Zero -> [(-. b) /. (2. *. a)]
  | Pos -> List.map [Float.sqrt discriminant; -. (Float.sqrt discriminant)]
                    ~f:(fun d -> ((-. b) +. d) /. (2. *. a))
      
let if_positive (a: float) : float option =
  match Float.sign_exn a with
  | Pos -> Some a
  | _ -> None
