open Base

type t = Color of int * int * int
                
let of_int (num: int) : t =
  let red = num / (256 * 256)
  and green = (num / 256) % 256
  and blue = num % 256 in
  Color (red, green, blue)

let of_tuple ((rr, gg, bb): int * int * int) : t =
  Color (rr, gg, bb)
  
let to_int (Color (rr, gg, bb): t) : int =
  256 * 256 * rr + 256 * gg + bb
  
let to_tuple (Color (rr, gg, bb): t) : int * int * int =
  (rr, gg, bb)
  
let of_percent ((rp, gp, bp): float * float * float) : t =
  Color (Float.to_int (rp *. 256.),
         Float.to_int (gp *. 256.),
         Float.to_int (bp *. 256.))
  
let to_percent (Color (rr, gg, bb): t) : float * float * float =
  ((Float.of_int rr) /. 256.,
   (Float.of_int gg) /. 256.,
   (Float.of_int bb) /. 256.) 

let ( *!) (color: t) (modif: float) : t =
  let (rr, gg, bb) = to_percent color in
  of_percent (modif *. rr, modif *. gg, modif *. bb)

let correct_gamma (color: t) : t =
  let (rr, gg, bb) = to_percent color in
  of_percent (Float.sqrt rr, Float.sqrt gg, Float.sqrt bb)
