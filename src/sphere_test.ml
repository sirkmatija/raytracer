open Base

let () =
  let unit_sphere = Sphere.make (Vec3.make 0. 0. 0.) 1. (Surface.make_material (Rgb.of_tuple (0xff, 0, 0)))
  and ray = Ray.make (Vec3.make 0. 0. 2.) (Vec3.make 0. 0. (-. 1.))
  in
  match Surface.intersection unit_sphere ray with
  | Some a ->
     assert Poly.(a = 1.);
     assert (Surface.is_member unit_sphere (Vec3.make 0. 1. 0.));
     assert Vec3.((Surface.normal unit_sphere (Vec3.make 0. 1. 0.)) =! (Vec3.make 0. 1. 0.)) 
  | None -> assert false
