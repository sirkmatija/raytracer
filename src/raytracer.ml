open Base

let random_point_in_sphere center radius =
  let rad = radius /. (Float.sqrt 3.)
  and (xmod, ymod, zmod) = (Random.float 1., Random.float 1., Random.float 1.) in
  Vec3.(center +! (make (xmod *. rad) (ymod *. rad) (zmod *. rad)))
   
let rec color ray scene =
  let intersects = List.map scene
                            ~f:(fun sphere -> (Surface.intersection sphere ray, sphere)) in
  let filtered = List.filter intersects
                             ~f:(fun (x, _) -> Option.is_some x) in
  let unpacked = List.map filtered
                          ~f:(fun (a, s) -> match a with
                                            | Some b -> (b, s)
                                            | None -> assert false) in
  let closest = List.hd (List.sort unpacked
                                   ~compare:(fun (x1, _) (x2, _) ->
                                     match Float.sign_exn (x1 -. x2) with
                                     | Pos -> 1
                                     | Zero -> 0
                                     | Neg -> -1)) in
  match closest with
  | None -> let v = Vec3.(y (unit_vec (Ray.direction ray))) in
            Rgb.of_percent ((1. -. 0.5 *. v),
                            (1. -. 0.3 *. v),
                            1.)
  | Some (t, s) -> let point = Ray.point_at_parameter ray t in
                   let normal = Surface.normal s point in
                   let random = random_point_in_sphere Vec3.(point +! normal) 1. in
                   let dir_color = color (Ray.make point Vec3.(random -! point)) scene
                   in Rgb.(dir_color *! 0.5)
                              
let () =
  let screenw = 1600
  and screenh = 800
  and maxs = 10
  and camera = Camera.make (Vec3.make (-. 16.) (-. 8.) (-. 8.))
                           (Vec3.make 32. 0. 0.)
                           (Vec3.make 0. 16. 0.)
                           (Vec3.make 0. 0. 0.)
  and scene = [Sphere.make (Vec3.make 0. 0. (-. 1.)) 0.5 (Surface.make_material (Rgb.of_int 0xff0000));
               Sphere.make (Vec3.make 0. (-. 100.5) (-. 1.)) 100. (Surface.make_material (Rgb.of_int 0x00ff00))]
  in
  let pixel_buffer = Matrix.make_fun
                       screenw
                       screenh
                       (fun x y ->
                         List.map (List.range 0 maxs)
                                  ~f:(fun _ ->
                                    let u = Float.(((Random.float 1.) +. (of_int x))
                                                   /. (of_int screenw))
                                    and v = Float.(((Random.float 1.) +. (of_int y))
                                                   /. (of_int screenh)) in
                                    let ray = Camera.get_ray camera u v in
                                    color ray scene)
                         |> List.reduce_exn ~f:(fun col1 col2 ->
                                              let (x, y, z) = Rgb.to_percent col1
                                              and (a, b, c) = Rgb.to_percent col2 in
                                              Rgb.of_percent (x +. a, y +.b, z +.c))
                         |> (fun col ->
                          let (x, y, z) = Rgb.to_percent col in
                          Rgb.correct_gamma (Rgb.of_percent (x /. (Float.of_int maxs),
                                                             y /. (Float.of_int maxs),
                                                             z /. (Float.of_int maxs)))))
  in
  Image.draw_image (Image.make pixel_buffer) "raytrace.ppm"
