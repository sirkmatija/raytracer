type material = {color: Rgb.t}

type t = {intersection: Ray.t -> float option;
          material: material;
          normal: Vec3.t -> Vec3.t;
          is_member: Vec3.t -> bool}

let make_material (color: Rgb.t) : material =
  {color}
       
let intersection (surface: t) (ray: Ray.t) : float option =
  (surface.intersection ray)

let material (surface: t) =
  surface.material

let normal (surface: t) (point: Vec3.t) : Vec3.t =
  surface.normal point

let is_member (surface: t) (point: Vec3.t) : bool =
  surface.is_member point
  
let make (intersection: Ray.t -> float option) (material: material) (normal: Vec3.t -> Vec3.t) (is_member: Vec3.t -> bool) : t =
  {intersection;
   material;
   normal;
   is_member}
