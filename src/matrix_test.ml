open Base

let () = 
  let ma = Matrix.make 10 5 3.14
  and mb = Matrix.make_fun 10 5 (fun x y -> x + y) in
  let mc = Matrix.map ma (fun x -> x +. 2.) in
  let md = Matrix.map mc (fun x -> x -. 2.) in
  assert(Matrix.(eql ma md (fun x y -> (Float.to_int x) = (Float.to_int y))));
  assert(Float.to_int mc.(5).(3) = 5);
  assert(mb.(5).(4) = 9);
  assert(not Matrix.(eql (mapi mb
                               (fun obj x y -> Float.of_int (x * y * obj)))
                         md
                         (fun x y -> (Float.to_int x) = (Float.to_int y))))
 
