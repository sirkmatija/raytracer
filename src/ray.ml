type t = {origin: Vec3.t; direction: Vec3.t}

let make (origin: Vec3.t) (direction: Vec3.t) : t =
  {origin; direction}

let make_unit (origin: Vec3.t) (direction: Vec3.t) : t =
  {origin; direction = Vec3.unit_vec direction}
    
let origin (ray: t) : Vec3.t =
  ray.origin

let direction (ray: t) : Vec3.t =
  ray.direction

let point_at_parameter (ray: t) (parameter: float) : Vec3.t =
  Vec3.(ray.origin +! (ray.direction *! parameter))

let (=!) (a: t) (b: t) : bool =
  let (ao, ad, bo, bd) = (a.origin, a.direction, b.origin, b.direction) in
  Vec3.((ao =! bo) && (ad =! bd))  
