open Base

(** Specification of image. **)
type t

(** Create image from matrix of integers.
First dimension of matrix should be x, second y.  **)
val make : (Rgb.t Matrix.t) -> t

(** Draw image to .ppm file.  **)
val draw_image : t -> string -> unit
                        
(** Display image on screen. **)
                                  (** val display_image : t -> int -> unit **)
