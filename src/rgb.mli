(** Specification of RGB color. **)
type t

(** integer -> color **)
val of_int : int -> t

(** (rr, gg, bb) -> color **)
val of_tuple : int * int * int -> t

(** color -> integer **)
val to_int : t -> int

(** color -> (rr, gg, bb) **)
val to_tuple : t -> int * int * int

(** (percentage red, percentage green, percentage blue) -> color 
Percentage should be float between 0 and 1, where 0 is black/empty and 1 is white/full. **)
val of_percent : float * float * float -> t

(** color -> (percentage red, percentage green, percentage blue) 
Percentage should be float between 0 and 1, where 0 is black/empty and 1 is white/full. **)
val to_percent : t -> float * float * float

(** Multiply the color by float. **)
val ( *! ) : t -> float -> t

val correct_gamma : t -> t
