open Base

type t = Vector of float * float * float

let make (x: float) (y: float) (z: float) : t =
  Vector (x, y, z)

let of_tuple ((x, y, z): float * float * float) : t =
  Vector (x, y, z)

let to_tuple (Vector (x, y, z): t) : float * float * float =
  (x, y, z)

let x (Vector (x, _, _): t) : float =
  x

let y (Vector (_, y, _): t) : float =
  y

let z (Vector (_, _, z): t) : float =
  z

let length (Vector (x, y, z): t) : float =
  Float.sqrt (x **. 2. +. y **. 2. +. z **. 2.)

let ( *! ) (Vector (a, b, c): t) (x: float) : t =
  Vector (a *. x, b *. x, c *. x)

let (/!) (v: t) (x: float) : t =
  v *! (1. /. x)

let unit_vec (v: t) : t =
  v /! (length v)

let (+!) (Vector (ax, ay, az): t) (Vector (bx, by, bz): t) : t =
  Vector (ax +. bx, ay +. by, az +. bz)

let (-!) (v: t) (u: t) : t =
  v +! (u *! (-. 1.))

let dot (Vector (ax, ay, az): t) (Vector (bx, by, bz): t) : float =
  ax *. bx +. ay *. by +. az *. bz

let cross (Vector (ax, ay, az): t) (Vector (bx, by, bz): t) : t =
  Vector (ay *. bz -. az *. by,
          az *. bx -. ax *. bz,
          ax *. by -. ay *. bx)
                                                           
let (=!) (Vector (vx, vy, vz): t) (Vector (ux, uy, uz): t) : bool =
  Poly.(vx = ux && vy = uy && vz = uz)
