open Base

type 'a t = 'a array array

let make (width: int) (height: int) (init: 'a) : 'a t =
  Array.make_matrix ~dimx:width ~dimy:height init

let mapi (matrix: 'a t) (fn: 'a -> int -> int -> 'b) : 'b t =
  Array.mapi matrix
             ~f:(fun x ary -> Array.mapi ary
                                         ~f:(fun y obj -> (fn obj x y)))
  
let make_fun (width: int) (height: int) (init_fun: int -> int -> 'a) : 'a t =
  let prototype = init_fun 0 0 in
  let matrix = make width height prototype in
  mapi matrix (fun _ x y -> (init_fun x y))

let width (matrix: 'a t) : int =
  Array.length matrix

let height (matrix: 'a t) : int =
  Array.length matrix.(0)

let iteri (matrix: 'a t) (fn: 'a -> int -> int -> unit): unit =
  Array.iteri matrix
              ~f:(fun x ary -> (Array.iteri ary
                                         ~f:(fun y obj -> (fn obj x y))))
  
let flatten (matrix: 'a t) : 'a array =
  let prototype = matrix.(0).(0) in
  let ary = Array.create ~len:((width matrix) * (height matrix)) prototype in
  iteri matrix (fun obj x y -> ary.(x + y) <- obj);
  ary
  
let eql (fst: 'a t) (snd: 'a t) (eq: 'a -> 'a -> bool) : bool =
  if (((width fst) = (width snd)) && ((height fst)) = (height snd)) then
    let eq_mask = mapi fst (fun obj x y -> (eq obj snd.(x).(y))) in
    let flat = flatten eq_mask in
    if (Array.exists ~f:(fun b -> not b) flat)
    then false
    else true
  else
    false

let map (matrix: 'a t) (fn: 'a -> 'b) : 'b t =
  Array.map matrix
            ~f:(fun ary -> Array.map ary
                                     ~f:fn)

let iter (matrix: 'a t) (fn: 'a -> unit) : unit =
  Array.iter matrix
             ~f:(fun ary -> Array.iter ary
                                       ~f:fn)
