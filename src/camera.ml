open Base

type t = {lower_left_corner: Vec3.t;
          hdir: Vec3.t;
          vdir: Vec3.t;
          eye: Vec3.t}

let make (lower_left_corner: Vec3.t) (hdir: Vec3.t) (vdir: Vec3.t) (eye: Vec3.t) : t =
  {lower_left_corner;
   hdir;
   vdir;
   eye}

let get_ray (camera: t) (h: float) (v: float) : Ray.t =
  Ray.make camera.eye Vec3.(camera.lower_left_corner 
                            +! (camera.hdir *! h) 
                            +! (camera.vdir *! v)) 
