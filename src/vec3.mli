(** 3d float vector. **)
type t

(** Create new vector from 3 floats. **)
val make : float -> float -> float -> t

(** Create a new vector from tuple of 3 floats. **)
val of_tuple : float * float * float -> t

(** Convert a vector to tuple of 3 floats. **)
val to_tuple : t -> float * float * float

(** Return x coordinate of vector. **)
val x : t -> float

(** Return y coordinate of vector. **)
val y : t -> float

(** Return z coordinate of vector. **)
val z : t -> float

(** Return length of vector. **)
val length : t -> float

(** Return unit vector with same properties as argument. **)
val unit_vec : t -> t

(** Sum of two vectors. **)
val (+!) : t -> t -> t

(** Difference of two vectors. **)
val (-!) : t -> t -> t

(** Product of vector and scalar. **)
val ( *! ) : t -> float -> t

(** Product of vector and 1 /. scalar. **)
val (/!) : t -> float -> t

(** Dot (scalar) product of two vectors. **)
val dot : t -> t -> float

(** Cross product of two vectors. **)
val cross : t -> t -> t

(** Compare two vectors for equality. **)
val (=!) : t -> t -> bool
