open Base

let sphere_normal (center: Vec3.t) (point: Vec3.t) : Vec3.t =
  Vec3.(unit_vec (point -! center))

let sphere_member (center: Vec3.t) (radius: float) (point: Vec3.t) : bool =
  let modif = Vec3.(point -! center) in
  let fst = Vec3.x modif
  and snd = Vec3.y modif
  and thr = Vec3.z modif
  in
  match Float.sign_exn (fst *. fst +. snd *. snd +. thr *. thr -. radius *. radius) with
  | Zero -> true
  | _ -> false

let sphere_intersection (center: Vec3.t) (radius: float) (ray: Ray.t) : float option =
  let (o, d) = (Ray.origin ray, Ray.direction ray) in
  (* Polynomial coefficients: *)
  let fst = (Vec3.dot d d)
  and snd = 2. *. (Vec3.(dot (o -! center) d))
  and thr = (Vec3.(dot (o -! center) (o -! center))) -. radius **. 2.
  in
  match Math.solve_quadratic fst snd thr with
  | [] -> None
  | [a] -> Math.if_positive a
  | a :: b :: [] -> let cand = Float.min a b in
                    (match Math.if_positive cand with
                     | None -> Math.if_positive (Float.max a b)
                     | a -> a)
  | _ -> assert false (* Quadratic shouldn't have more than two solutions in reals. *)
       
let make (center: Vec3.t) (radius: float) (material: Surface.material) : Surface.t =
  Surface.make (sphere_intersection center radius) material (sphere_normal center) (sphere_member center radius)
