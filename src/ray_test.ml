open Base
open Ray
       
let () =
  let ray = make
              (Vec3.make 1. 1. 1.)
              (Vec3.make 2. 0. 0.)
  and ray' = make
               (Vec3.make 1. 1. 1.)
               (Vec3.make 2. 0. 0.)
  and unitray = make_unit
                  (Vec3.make 0. 0. 2.)
                  (Vec3.make 3. 5. 0.567)
  in
  assert ((Float.to_int (Vec3.x (origin ray))) = 1);
  assert ((Float.to_int (Vec3.y (direction ray))) = 0);
  assert ((Float.to_int (Vec3.x (point_at_parameter ray 4.))) = 9);
  assert ((Float.to_int (Vec3.length (direction unitray))) = 1);
  assert (ray =! ray')
