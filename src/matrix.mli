(** 2d array type. **)
type 'a t = 'a array array

(** make width height init -> matrix of size width * height
with all elements initialized to init **)
val make : int -> int -> 'a -> 'a t

(** make_fun width height init_fun -> matrix of size width * height
with element x,y initialized to result of callin init_fun x y. **)   
val make_fun : int -> int -> (int -> int -> 'a) -> 'a t

(** True if matrices hold same elements, else false. **)
val eql : 'a t -> 'a t -> ('a -> 'a -> bool) -> bool

(** Return width of matrix. **)
val width : 'a t -> int

(** Return height of matrix. **)
val height : 'a t -> int
  
(** Flattens matrix 'a t into 'a array. **)
val flatten : 'a t -> 'a array
  
(** Like Array.map. **)
val map : 'a t -> ('a -> 'b) -> 'b t

(** Like Array.mapi. **)
val mapi : 'a t -> ('a -> int -> int -> 'b) -> 'b t

(** Like Array.iter. **)
val iter : 'a t -> ('a -> unit) -> unit

(** Like Array.iteri. **)
val iteri : 'a t -> ('a -> int -> int -> unit) -> unit
