type t

val make : Vec3.t -> Vec3.t -> Vec3.t -> Vec3.t -> t

val get_ray : t -> float -> float -> Ray.t
