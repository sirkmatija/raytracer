open Base

let () =
  let (a1, b1, c1) = (5., 4., 1.)
  and (a2, b2, c2) = (2., 4., 2.)
  and (a3, b3, c3) = ((-. 2.), 3., 5.)
  in
  let q1 = Math.solve_quadratic a1 b1 c1
  and q2 = Math.solve_quadratic a2 b2 c2
  and q3 = Math.solve_quadratic a3 b3 c3
  in
  assert ((List.length q1) = 0);
  assert ((List.length q2) = 1);
  assert ((List.length q3) = 2);
  match List.hd q2 with
  | None -> assert false
  | Some car -> assert ((Float.to_int car) = -1)
