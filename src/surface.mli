(** Surface type. **)
type t

type material = {color: Rgb.t}

val make_material : Rgb.t -> material
              
(** Create a new surface. **)
val make : (Ray.t -> float option) -> material -> (Vec3.t -> Vec3.t) -> (Vec3.t -> bool) -> t
   
(** intersection surface ray should return parameter,
at which ray intersects surface. **)
val intersection : t -> Ray.t -> float option

(** Returns color of surface. **)
val material : t -> material

(** Return normal vector at point. **)
val normal : t -> Vec3.t -> Vec3.t

(** Return true if point lies at surface, else false. **)
val is_member : t -> Vec3.t -> bool
