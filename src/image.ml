open Base
open Stdio
       
type t = {w: int; h: int; contents: Rgb.t Matrix.t}

let make (matrix: Rgb.t Matrix.t) : t =
  let contents =
    Array.transpose_exn matrix
  in {contents; w = (Matrix.height contents); h = (Matrix.width contents)}
       
let draw_image (image: t) (filename: string) : unit =
  let file = Out_channel.create filename in
  (* Header. *)
  Out_channel.fprintf file "P3\n%d %d\n255\n" image.w image.h;
  
  (* Painting the dots. *)
  let contents = Array.copy image.contents in
  
  (* We have to turn it around, since for us 0,0 is bottom left
corner, while without reversing it's in ppm upper left. *)
  Array.rev_inplace contents;

  Matrix.iter contents
              (fun color -> let (rr, gg, bb) = Rgb.to_tuple color in
                             Out_channel.fprintf file "%d %d %d\n" rr gg bb);
  Out_channel.close file
                    

(** Possible error in Ocaml.Graphics library. **)
  (*
let display_image (image: t) (delay: int) : unit =
  Graphics.open_graph "";
  Graphics.resize_window image.w image.h;
  
  let matrix = image.contents in
  Matrix.iteri matrix
               (fun rgb x y -> Graphics.set_color (Rgb.to_int rgb);
                               Graphics.plot x y);
  Unix.sleep delay;
  Graphics.close_graph ()
   *)
