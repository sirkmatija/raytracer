(** Solves equation ax2 + bx + c = 0 and returns
real results in list. **)
val solve_quadratic : float -> float -> float -> float list

(** Return Some arg, if argument is positive, else None. **)
val if_positive : float -> float option
