Modules:
- Rgb -> rgb color type and associated operations
- Image -> pixel matrix type, drawing and other operations
- Vec3 -> 3d vector of floats with usual operations (+, scalar
multiplication, dot product, cross product, ...)
- Ray -> p(t) = origin_vec3 + t * direction_vec3 data type and
associated operations
- Matrix -> 2d array type with associated operations
- Surface -> 3d object hittable by ray type.

Conventions:
Principal type in module should be named t.
Its constructor should be make.
Arithemic infix operations end with !.
Concrete implementation shouldn't be exposed, except rarely.
Functions must not mutate inputs.